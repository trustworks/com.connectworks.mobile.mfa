import 'package:flutter/material.dart';
import 'package:connectworks/auth.dart';
import 'package:connectworks/common/deviceUtils.dart';
import 'package:connectworks/data/database_helper.dart';
import 'package:connectworks/data/rest_ds.dart';
import 'package:connectworks/model/deviceInfo.dart';
import 'package:connectworks/presenter/LoginScreenPresenter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:swagger/api.dart';

class LoginScreen extends StatefulWidget {
  static const String USERNAME = 'USERNAME';
  static const String PASSWORD = 'PASSWORD';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    implements LoginScreenContract, AuthStateListener {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();

  bool _loginEnabled = false;

  String _password = '';
  String _username = '';

  BuildContext _ctx;

//  String _password = 'Demouser5';
//  String _username = 'docpackdemo1@gmail.com';
//  String _password = 'password123';
//  String _username = 'miki234@poczta.onet.pl';

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final bool _registrationAllowed = false;

  SharedPreferences prefs;

  bool _isLoading = false;
  LoginScreenPresenter _presenter;

  _LoginScreenState() {
    _presenter = LoginScreenPresenter(this);

    AuthStateProvider auth = AuthStateProvider();
    auth.subscribe(this);
  }

  void checkIfFieldsNeedToBeFilled() async {
    SharedPreferences.getInstance().then((onValue) async {
      prefs = onValue;

      if (prefs.getKeys().contains(LoginScreen.USERNAME)) {
        usernameController.text = prefs.getString(LoginScreen.USERNAME);
      }

      await DeviceUtils().init().then((DeviceInfo info) {
        print(info.toString());

        if (info.isEmulator) {
          if (info.OSName == 'google') {
            RestDataSource.BASE_URL = RestDataSource.BASE_ANDROID_EMULATOR_URL;
          } else {
            RestDataSource.BASE_URL = RestDataSource.BASE_IOS_EMULATOR_URL;
          }
        } else {
          RestDataSource.BASE_URL = RestDataSource.BASE_DEVICE_URL;
        }

        if (info.isEmulator) {
          if (prefs.getKeys().contains(LoginScreen.PASSWORD)) {
            passwordController.text = prefs.getString(LoginScreen.PASSWORD);
          }
        }
      });
    });
  }

  @override
  void initState() {
    usernameController.addListener(_checkIfLoginEnable);
    passwordController.addListener(_checkIfLoginEnable);
    checkIfFieldsNeedToBeFilled();
    super.initState();
  }

  void _checkIfLoginEnable() {
    setState(() {
      _loginEnabled = usernameController.text.isNotEmpty &&
          passwordController.text.isNotEmpty;
    });
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;

    var indicator = Container(
        height: 20,
        width: 20,
        margin: EdgeInsets.all(5),
        child: CircularProgressIndicator());

    var loginBtn = RaisedButton(
      key: Key('loginBtn'),
      onPressed: _submit,
      child: _isLoading
          ? indicator
          : Text(
              "Log in",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
      textColor: Colors.white,
      color: Color.fromRGBO(116, 169, 247, 1.0),
    );

    var registerBtn = RaisedButton(
      key: Key('registerBtn'),
      onPressed: _register,
      child: Text(
        "Register and accept",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      textColor: Colors.white,
      color: Color.fromRGBO(116, 169, 247, 1.0),
    );

    var loginForm = Column(
      children: <Widget>[
        Text(
          "Login to Connectworks",
          textScaleFactor: 1.4,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                child: TextFormField(
                  key: Key('emailKey'),
                  autocorrect: false,
                  controller: usernameController,
                  onSaved: (val) => _username = val,
                  decoration: InputDecoration(
                    labelText: "Email",
                    icon: Icon(
                      IconData(57534, fontFamily: 'MaterialIcons'),
                      color: Colors.grey,
//                  size: 20.0,
                    ),
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                  child: TextFormField(
                    key: Key('passwordKey'),
                    autocorrect: false,
                    controller: passwordController,
                    onSaved: (val) => _password = val,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "Password",
                      icon: Icon(
                        IconData(59543, fontFamily: 'MaterialIcons'),
                        color: Colors.grey,
//                  size: 20.0,
                      ),
                    ),
                  )),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
          child: SizedBox(
            child: loginBtn,
            height: 40.0,
            width: 100.0,
          ),
        )
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
    );

    var tabBar = TabBar(
      labelColor: Colors.black,
      labelStyle: TextStyle(fontSize: 24, fontWeight: FontWeight.w300),
      indicator: BoxDecoration(
        color: Color.fromRGBO(250, 250, 250, 1.0),
      ),
      onTap: (idx) {
        if (idx == 0 && _registrationAllowed == false) {
          return;
        }

        print(idx);
      },
      tabs: <Widget>[
        Tab(
          child: Text("Register"),
        ),
        Tab(
          child: Text("Login"),
        ),
      ],
    );

    var loginTab = Center(
      child: Container(
        child: loginForm,
        height: 240.0,
        width: 300.0,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
                color: Color(0xAA808080),
                offset: Offset(3.0, 3.0),
                blurRadius: 6.0)
          ],
        ),
      ),
    );

//    var scaffold = Scaffold(
//      backgroundColor: Color.fromRGBO(244, 244, 244, 1.0),
//      key: scaffoldKey,
//      body: loginTab,
//    );

    var registerForm = Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
          child: Text(
            "You have been invited to join Connectworks. If you would like to accept then please register.",
            style: TextStyle(fontWeight: FontWeight.w200),
          ),
        ),
        Form(
          child: Column(
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                      child: TextFormField(
                        autocorrect: false,
                        decoration: InputDecoration(
                          labelText: "First name",
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 20.0, 0.0),
                      child: TextFormField(
                        autocorrect: false,
                        decoration: InputDecoration(
                          labelText: "Last name",
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                child: TextFormField(
                  autocorrect: false,
                  controller: usernameController,
                  onSaved: (val) => _username = val,
                  decoration: InputDecoration(
                    labelText: "Email",
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                  child: TextFormField(
                    autocorrect: false,
                    controller: passwordController,
                    onSaved: (val) => _password = val,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "Password",
                    ),
                  )),
              Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                  child: TextFormField(
                    autocorrect: false,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "Confirm password",
                    ),
                  )),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
          child: SizedBox(
            child: registerBtn,
            height: 40.0,
          ),
        )
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
    );

    var registerTab = Center(
      child: Container(
        child: registerForm,
        height: 440.0,
        width: 360.0,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
                color: Color(0xAA808080),
                offset: Offset(3.0, 3.0),
                blurRadius: 6.0)
          ],
        ),
      ),
    );

    var tabBarView = TabBarView(
      children: <Widget>[
        registerTab,
        loginTab,
      ],
    );

    var logo = Padding(
      padding: const EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 20.0),
      child: Image.asset("assets/LoginLogo.png"),
    );

    var scaffold = Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: logo,
          backgroundColor: Color.fromRGBO(236, 236, 236, 1.0),
          elevation: 0,
          bottom: tabBar,
        ),
        body: tabBarView);

    return DefaultTabController(
      length: 2,
      initialIndex: 1,
      child: scaffold,
    );
  }

  void _submit() async {
    if (!_loginEnabled) {
      return;
    }

    final form = this.formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();

      _presenter.doLogin(_username, _password);

      await prefs.setString(LoginScreen.USERNAME, usernameController.text);
      await prefs.setString(LoginScreen.PASSWORD, passwordController.text);
    }
  }

  void _register() {
//    _presenter.register('http://localhost.charlesproxy.com:8080/com.trustworks.studio.web/mobile/UWqxtvWTTAoZyGcTsOUwNflE8xo/accept.htm');
    _presenter.register();
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(text),
    ));
  }

  @override
  void onLoginError(String errorTxt) {
    _showSnackBar(errorTxt);
    setState(() => _isLoading = false);
  }

  @override
  void onLoginSuccess(UserAccountDetail user) async {
    _showSnackBar(user.toString());
    setState(() => _isLoading = false);

    var db = DatabaseHelper();
    await db.saveUser(user);

//    Future.delayed(const Duration(milliseconds: 2000), () {
    AuthStateProvider auth = AuthStateProvider();
    auth.notify(AuthState.LOGGED_IN);
//    });
  }

  @override
  void onAuthStateChange(AuthState state) {
    if (state == AuthState.LOGGED_IN) {
      Navigator.of(_ctx).pushReplacementNamed("/home");
    }
  }

  @override
  void onRegisterError(String errorTxt) {
    _showSnackBar(errorTxt);
  }

  @override
  void registerSuccess(param) {
    _showSnackBar(param.toString());
  }
}
