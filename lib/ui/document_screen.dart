import 'package:connectworks/presenter/documentPresenter.dart';
import 'package:flutter/material.dart';
import 'package:swagger/api.dart';

class DocumentScreen extends StatefulWidget {

  @override
  _DocumentScreenState createState() => _DocumentScreenState();
}

class _DocumentScreenState extends State<DocumentScreen>
    implements DocumentContract {
  DocumentPresenter _presenter;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  _DocumentScreenState() {
    _presenter = DocumentPresenter(this);
  }

  void _getDocsById() {
    _presenter.getDocumentById(845497);
  }

  void _getDocs() {
    _presenter.getDocuments();
  }

  void _createTestDocs() {
    _presenter.createTestDocument('test from mobile');
  }

  void _getSignedUrl(){
    _presenter.getSignedUrlById(845497);
  }


  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(text),
    ));
  }

  RaisedButton _createBtn(String label, Function pressCallback, {String key}) {
    return RaisedButton(
      key: (key!=null) ? Key(key) : null,
      onPressed: pressCallback,
      child: Text(
        label,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      textColor: Colors.white,
      color: Color.fromRGBO(116, 169, 247, 1.0),
    );
  }

  @override
  Widget build(BuildContext context) {

    RaisedButton getSignedByIdBtn = _createBtn("Get Signed Url By ID", _getSignedUrl);
    RaisedButton getDocsByIdBtn = _createBtn( "Get Documents By ID", _getDocsById);
    RaisedButton getDocsBtn = _createBtn("Get Documents", _getDocs);
    RaisedButton createTestDocsBtn = _createBtn("Create Test Doc", _createTestDocs, key: 'createTestDocsBtn');

    return Scaffold(
      backgroundColor: Color.fromRGBO(244, 244, 244, 1.0),
      key: scaffoldKey,
      body: Center(
        child: Column(
          children: <Widget>[
            Text("Document API test"),
            getDocsByIdBtn,
            getDocsBtn, 
            createTestDocsBtn,
            getSignedByIdBtn
          ],
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
        ),
      ),
    );
  }

  @override
  void getDocumentSuccess(List<Record> records) {
    _showSnackBar(records.toString());
  }

  @override
  void onGetDocumentError(String errorTxt) {
    _showSnackBar(errorTxt);
  }

  @override
  void createDocumentSuccess(DocumentRecordCreate record) {
    _showSnackBar(record.toString());
  }

  @override
  void createDocumentError(String errorTxt) {
    _showSnackBar(errorTxt);
  }

  @override
  void getDocumentByIdSuccess(Record record) {
    _showSnackBar(record.toString());
  }

  @override
  void onGetDocumentByIdError(String errorTxt) {
    _showSnackBar(errorTxt);
  }

  @override
  void getSignedUrlByIdSuccess(RecordUrl recordUrl) {
    _showSnackBar(recordUrl.toString());
  }

  @override
  void onSignedUrlByIdError(String errorTxt) {
    _showSnackBar(errorTxt);
  }
  
}
