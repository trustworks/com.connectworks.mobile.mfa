import 'package:connectworks/poc.dart';
import 'package:flutter/material.dart';
import 'package:connectworks/routes.dart';
import 'package:connectworks/utils/network_util.dart';
import 'package:provider/provider.dart';
import 'dart:io';

import 'bloc.dart';

void main() {

  // to make it work with Charles Proxy tool works only on ios simulator as it is localhost
  HttpOverrides.global = MyProxyHttpOverride();

  runApp(new LoginApp());
}

class LoginApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Login',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          accentColor: Colors.white
      ),
      routes: routes,
    );
  }

//  @override
//  Widget build(BuildContext context) {
//    DeepLinkBloc _bloc = DeepLinkBloc();
//    return MaterialApp(
//        debugShowCheckedModeBanner: false,
//        title: 'Flutter and Deep Linsk PoC',
//        theme: ThemeData(
//            primarySwatch: Colors.blue,
//            textTheme: TextTheme(
//              title: TextStyle(
//                fontWeight: FontWeight.w300,
//                color: Colors.blue,
//                fontSize: 25.0,
//              ),
//            )),
//        home: Scaffold(
//            body: Provider<DeepLinkBloc>(
//                create: (context) => _bloc,
//                dispose: (context, bloc) => bloc.dispose(),
//                child: PocWidget())));
//  }
}
