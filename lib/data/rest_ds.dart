import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:connectworks/utils/network_util.dart';
import 'package:http/http.dart';
import 'package:oauth2/oauth2.dart' as oauth2;
import 'package:path_provider/path_provider.dart';
import 'package:swagger/api.dart';

class RestDataSource {

  static final RestDataSource _instance = RestDataSource.internal();
  factory RestDataSource() => _instance;
  RestDataSource.internal();

  NetworkUtil _netUtil = NetworkUtil();

  static final String API_KEY = "353b302c44574f565045687e534e7d6a";
  static final String API_SECRET = "286924697e615a672a646a493545646c";

  static final String CTX = "/com.trustworks.studio.web";
  static final String TOKEN_SERVER_URL = "oauth/token";
  static final String BASE_DEVICE_URL = "http://192.168.1.57:8080";
  static final String BASE_ANDROID_EMULATOR_URL = "http://10.0.2.2:8080";
  static final String BASE_IOS_EMULATOR_URL = "http://localhost.charlesproxy.com:8080";
  static String BASE_URL = "";

  ApiClient api;

  Future<UserAccountDetail> login(String username, String password) async {

    api = ApiClient(basePath: '$BASE_URL$CTX/api/v2');
//    print('basePath: $BASE_URL$CTX/api/v2');

    final authorizationEndpoint =  Uri.parse('$BASE_URL$CTX/$TOKEN_SERVER_URL');
//    print('authPath: $BASE_URL$CTX/$TOKEN_SERVER_URL');

    oauth2.Client client = await oauth2.resourceOwnerPasswordGrant(
        authorizationEndpoint, '$username', '$password',
        identifier: API_KEY, secret: API_SECRET);

    api.setAccessToken(client.credentials.accessToken);

    UserApi apiInstance = UserApi(api);

    return apiInstance.userGetUsingGET('/').then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> register() async {
//    static final String REGISTER_URL = "http://localhost.charlesproxy.com:8080/com.trustworks.studio.web/mobile/invite/accept";
    String registerUrl = 'http://localhost.charlesproxy.com:8080/com.trustworks.studio.web/mobile/BeLNd7d8ebkCV3w_qyHbGVa3TIM/accept';
//    _netUtil.post(registerUrl, body: {
//      "code": 'UWqxtvWTTAoZyGcTsOUwNflE8xo',
//    }).then((dynamic res) {
//      print(res.toString());
//      return null;
//    });

    _netUtil.get(registerUrl).then((dynamic res) {
      print(res.toString());
      return null;
    });
  }

  Future<DocumentRecordCreate> createTestDocument(String title) async{

    RecordsApi apiInstance = RecordsApi(api);
    List<String> signatories = List<String>();
    signatories.add('test');

    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File f = File('$tempPath/contacts.txt');
    f.writeAsStringSync('test from mobile device');
    MultipartFile file = await MultipartFile.fromPath('file', f.path, filename: 'contacts.txt');

    return apiInstance.recordsUploadUsingPOSTUsingPOST('/4887/663075', title, signatories:signatories, file: file).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }


  Future<RecordUrl> getSignedUrlById(int recordId) async{

    RecordsApi apiInstance = RecordsApi(api);

    return apiInstance.recordsGetSignedUrlByIdUsingGET('/4887', recordId, 100, fileName:'temp.pdf').then((dynamic res) {
      print(res.toString());
      return res;
    });
  }


  Future<List<Record>> getDocuments() async{

    RecordsApi apiInstance = RecordsApi(api);

    return apiInstance.recordsGetUsingGET('/4887').then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<Record> getDocumentsById(int recordId) async{

    RecordsApi apiInstance = RecordsApi(api);

    return apiInstance.recordsGetByIdUsingGET('/4887', recordId)
        .then((dynamic res) {
      print(res.toString());
      return res;
    });
  }




}
