import 'dart:io' as io;
import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:swagger/api.dart';

class DatabaseHelper {
  // next three lines makes this class a Singleton
  static final DatabaseHelper _instance = DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  DatabaseHelper.internal();

  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }

    _db = await initDb();
    return _db;
  }

  Future<Database> initDb() async {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, "main.db");
//    print('$path');
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  void _onCreate(Database db, int version) async {
    // when creating the db, create the table
    String sql =
        'CREATE TABLE UserAccountDetail(id INTEGER PRIMARY KEY, email TEXT, administrator BOOLEAN)';
    await db.execute(sql);
  }

  Future<int> saveUser(UserAccountDetail user) async {
    Database dbClient = await db;
    int res = await dbClient.insert('UserAccountDetail', user.toJsonTest());
    return res;
  }

  Future<int> deleteUsers() async {
    Database dbClient = await db;
    int res = await dbClient.delete("UserAccountDetail");
    return res;
  }

  Future<bool> isLoggedIn() async{
    Database dbClient = await db;
    dynamic res = await dbClient.query("UserAccountDetail");
    return res.length > 0 ? true : false;
  }
}
