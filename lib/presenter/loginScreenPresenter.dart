import 'package:connectworks/data/rest_ds.dart';
import 'package:swagger/api.dart';

abstract class LoginScreenContract {
  void onLoginSuccess(UserAccountDetail user);
  void onLoginError(String errorTxt);

  void registerSuccess(dynamic param);
  void onRegisterError(String errorTxt);
}

class LoginScreenPresenter {
  LoginScreenContract _view;
  LoginScreenPresenter(this._view);
  RestDataSource api = RestDataSource();


  void register() {
    api.register().then((dynamic param) {
      _view.registerSuccess(param);
    }).catchError((dynamic error) {
      _view.onRegisterError(error.toString());
    });
  }

  void doLogin(String username, String password) {
    api.login(username, password).then((UserAccountDetail user) {
      _view.onLoginSuccess(user);
    }).catchError((dynamic error) {
      _view.onLoginError(error.toString());
    });
  }
}
