import 'package:connectworks/data/rest_ds.dart';
import 'package:swagger/api.dart';

abstract class DocumentContract {
  void getDocumentSuccess(List<Record> records);
  void onGetDocumentError(String errorTxt);

  void createDocumentSuccess(DocumentRecordCreate record);
  void createDocumentError(String errorTxt);

  void getDocumentByIdSuccess(Record record);
  void onGetDocumentByIdError(String errorTxt);

  void getSignedUrlByIdSuccess(RecordUrl recordUrl);
  void onSignedUrlByIdError(String errorTxt);
}

class DocumentPresenter {
  DocumentContract _view;
  DocumentPresenter(this._view);
  RestDataSource api = RestDataSource();

  getDocuments() {
    api.getDocuments().then((List<Record> records) {
      _view.getDocumentSuccess(records);
    }).catchError((dynamic error) {
      _view.onGetDocumentError(error.toString());
    });
  }

  getDocumentById(int recordId) {
    api.getDocumentsById(recordId).then((Record record) {
      _view.getDocumentByIdSuccess(record);
    }).catchError((dynamic error) {
      _view.onGetDocumentByIdError(error.toString());
    });
  }

  getSignedUrlById(int recordId) {
    api.getSignedUrlById(recordId).then((RecordUrl recordUrl) {
      _view.getSignedUrlByIdSuccess(recordUrl);
    }).catchError((dynamic error) {
      _view.onSignedUrlByIdError(error.toString());
    });
  }

  createTestDocument(String title){
    api.createTestDocument(title).then((dynamic res) {
      _view.createDocumentSuccess(res);
    }).catchError((dynamic error) {
      _view.createDocumentError(error.toString());
    });
  }
}
