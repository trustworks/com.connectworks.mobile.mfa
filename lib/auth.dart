
import 'package:connectworks/data/database_helper.dart';

enum AuthState{LOGGED_IN, LOGGED_OUT}

abstract class AuthStateListener{
  void onAuthStateChange(AuthState state);
}

// naive implementation of Observer/Subscriber pattern
class AuthStateProvider{

  // next three lines makes this class a Singleton
  static final AuthStateProvider _instance = AuthStateProvider.internal();
  factory AuthStateProvider() => _instance;

  List<AuthStateListener> _subscribers;

  AuthStateProvider.internal(){
    // constructor
    _subscribers = List<AuthStateListener>();
    initState();
  }

  void subscribe(AuthStateListener listener){
    _subscribers.add(listener);
  }

  void initState() async {
    DatabaseHelper dh = DatabaseHelper();
    bool isLoggedIn = await dh.isLoggedIn();
    if(isLoggedIn){
//      notify(AuthState.LOGGED_IN);
    }else{
//      notify(AuthState.LOGGED_OUT);
    }
  }

  void dispose(AuthStateListener listener){
    for(dynamic l in _subscribers){
      if(l == listener){
        _subscribers.remove(l);
      }
    }
  }

  void notify(AuthState state){
    _subscribers.forEach((AuthStateListener s) => s.onAuthStateChange(state));
  }

}