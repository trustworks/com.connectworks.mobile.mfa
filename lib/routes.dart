import 'package:flutter/cupertino.dart';
import 'package:connectworks/ui/document_screen.dart';
import 'package:connectworks/ui/login_screen.dart';


final routes = {
  '/login': (BuildContext ctx) => LoginScreen(),
  '/home': (BuildContext ctx) => DocumentScreen(),
  '/': (BuildContext ctx) => LoginScreen(),

};