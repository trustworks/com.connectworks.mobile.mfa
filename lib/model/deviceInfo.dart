class DeviceInfo {
  String DeviceID;
  String Model;
  String OSName;
  String OSVersion;
  bool isEmulator;

  @override
  String toString() =>
      '(DeviceID:$DeviceID Model:$Model OSName:$OSName OSVersion:$OSVersion isEmulator:$isEmulator)';
}
